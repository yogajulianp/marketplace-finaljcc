import { NavigationContainer } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Alert } from "react-native";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Button,
} from "react-native";
import { Data } from "./data";
import firebase from "firebase/app";
import "firebase/auth";
import 'firebase/firestore';

export default function Home() {
  // const [title, setTitle]=useState("");
  // const [image, setHarga]=useState("");
  // const [desc, setDesc]=useState("");
  // const [items, setItems]=useState([]);
  // const [button, setButton]=useState("Simpan");
  // const [selectedUser, setSelectedUser] = useState({});

  const [user, setUser] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);


  useEffect(() => {
    const userInfo = firebase.auth().currentUser;
    setUser(userInfo);
  }, []);

  const currencyFormat = (num) => {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  const db = firebase.firestore();

  // firebase.auth().onAuthStateChanged((user) => {
  //   if (_user) {
  //     // User is signed in, see docs for a list of available properties
  //     // https://firebase.google.com/docs/reference/js/firebase.User
  //     var uid = user.uid;
  //     setUser(_user),
  //     // ...
  //   } else {
  //     // User is signed out
  //     // ...
  //   }
  // });

  const updateHarga = (price, item) => {
    console.log("UpdatPrice : " + price);
    const temp = Number(price) + totalPrice;
    console.log(temp);
    setTotalPrice(temp);
    const user = firebase.auth().currentUser;
    db.collection("cart")
      .add({
        ...item,
        user_id: user.uid,
      })
      .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
      })
      .catch((error) => {
        console.error("Error adding document: ", error);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.TopText}>
        <Image
          style={styles.logoDrawwer}
          source={require("./images/Logo.png")}
        />
        <Text>Home</Text>
      </View>

      <View style={styles.Cover}></View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          padding: 16,
        }}
      >
        <View style={{ paddingHorizontal: 10 }}>
          <Text>Selamat Datang,</Text>
          <Text style={{ fontSize: 12, fontWeight: "bold" }}>
            Hallo, {user.email}
          </Text>
        </View>

        <View style={{ paddingHorizontal: 10 }}>
          <Text>Total Harga:</Text>
          <Text style={{ fontSize: 18, fontWeight: "bold" }}>
            {" "}
            {currencyFormat(totalPrice)}
          </Text>
        </View>
      </View>

      <SafeAreaView style={styles.homebox}>
        <FlatList
          data={Data}
          keyExtractor={(item) => item.id}
          numColumns={2}
          renderItem={({ item }) => {
            return (
              <View style={styles.content}>
                <View style={styles.boxProduct}>
                  <View style={styles.isiBox}>
                    <Text>{item.title}</Text>
                    <Image
                      style={{ height: 100, width: 100, marginRight: 5 }}
                      source={item.image}
                    />
                    <Text>{`Rp ${item.harga}`}</Text>
                    <Text>{item.type}</Text>
                    <TouchableOpacity>
                      <View style={styles.orderButton}>
                        <Button
                          title={"BELI"}
                          onPress={() => {
                            updateHarga(item.harga, item);
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>

                {/* <View
                  style={{ borderBottomWidth: 1, borderBottomColor: "#A8AAAB" }}
                /> */}
              </View>
            );
          }}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#48EBEB",
  },
  TopText: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    alignItems: "flex-end",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    flexDirection: "row",
  },
  logoDrawwer: {
    height: 60,
    width: 120,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 40,
  },
  cover: {
    flex: 1,
  },
  homebox: {
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "row",
    paddingTop: 20,
    paddingHorizontal: 20,
    paddingBottom: 100,
  },
  content: {
    width: 150,
    height: 220,
    margin: 5,
    borderWidth: 1,
    alignItems: "center",
    borderRadius: 5,
    borderColor: "grey",
  },
  boxProduct: {
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
    paddingBottom: 20,
    paddingTop: 10,
  },

  orderButton: {
    alignItems: "center",
    justifyContent: "center",
  },
  isiBox: {
    justifyContent: "center",
    alignItems: "center",
  },
});
